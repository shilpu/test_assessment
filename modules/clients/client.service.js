var Clients = require('./client.model');
var Promise = require('promise');
module.exports = {
	getNumber: function(data){
		return new Promise(function (resolve, reject) {
			Clients.findOne(data, function (err, response) {
				if(err){
					reject(err);
				}
				else{
					resolve(response);
				}
			});
		})
	},
	saveNumber: function (data) {
		return new Promise(function (resolve, reject) {
			var number = new Clients(data);
			number.save(function (err, response) {
				if(err){
					reject(err);
				}
				else {
					console.log("response", response);
					resolve(response);
				}
			})
		})
		
		 
	}
} 