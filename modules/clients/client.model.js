var mongoose = require('mongoose');
var schema = mongoose.Schema;
var numberSchema = new schema({
	number : {
		type:String
	},
	created_date:{
		type:Date,
		default: Date.now()
	}
})
module.exports = mongoose.model('Numbers',numberSchema);