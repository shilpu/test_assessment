var apiCodes = require('../../config/apicodes');
var messages = require('../../config/messages');
var clientService = require('./client.service');


module.exports = function (app) {
	app.getNumbers = function (req, res) {
		checkNumberExist(req, res).then((response) => {
			if (response && response.number) {
				clientService.saveNumber({
					number: response.number
				}).then((response) => {
;					if (response) {
						return res.status(apiCodes.SUCCESS).send({
							number: response.number
						});
					}
				}).catch((err) => {
					return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({
						message: messages.ERROR_WHILE_SAVING
					});
				})
			}
		}).catch((err) => {
			return res.status(apiCodes.INTERNAL_SERVER_ERROR).send({
				message: messages.INTERNAL_SERVER_ERROR
			});
		})

	}

	function generateRandomNumber(min, max) {
		return (Math.floor(Math.random() * max) + min);
	}

	function getTelNumber() {
		let telNumber = generateRandomNumber(111, 999) + '-' + generateRandomNumber(111, 999) + '-' + generateRandomNumber(1111, 9999);
		return telNumber;
	}

	function checkNumberExist(req, res) {
		let number = getTelNumber();
		return new Promise((resolve) => {
			clientService.getNumber({
				number: number
			}).then((response) => {
				if (response) {
					getNumbers(req, res)
				} else {
					return resolve({
						number: number
					});
				}
			})
		})
	}
}
