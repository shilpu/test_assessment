module.exports = {
  	"INTERNAL_SERVER_ERROR":500,
    "BAD_REQUEST" : 400,
    "UNAUTHORIZED" : 401,
    "FORBIDDEN" : 403 ,
    "DUPLICATE": 409,
    "NOT_FOUND" : 404,
    "SUCCESS" : 200
}